# xposed-extention

#### 项目介绍
对xposed API的一些功能做扩展，我在使用xposed过程中总结的一个写工具类

抽取工具类的主要目的是，我有三个base on xposed的项目，在使用xpsoed的时候，由于各种使用反射操作代码，很难让代码比较优雅，所以慢慢抽取了这些工具类

使用方式：

gradle:
```
compile 'com.virjar:xposed-extention:1.0.7'
```

maven:
```
<dependency>
  <groupId>com.virjar</groupId>
  <artifactId>xposed-extention</artifactId>
  <version>1.0.7</version>
  <type>aar</type>
</dependency>
```

如果是snapshot的版本的话，需要依赖maven的snapshot仓库
```
maven {
      name "contralSnapshot"
      url "https://oss.sonatype.org/content/repositories/snapshots/"
}
```