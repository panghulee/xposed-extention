package com.virjar.xposed_extention;

import com.google.common.collect.Maps;
import com.google.common.collect.Sets;

import java.util.Set;
import java.util.concurrent.ConcurrentMap;

public class Ones {
    private static ConcurrentMap<Class<?>, Set<String>> completedTasks = Maps.newConcurrentMap();

    public interface DoOnce {
        void doOne(Class<?> clazz);
    }

    public static boolean hookOnes(Class<?> clazz, String taskType, DoOnce doOnce) {
        Set<String> tasks = completedTasks.get(clazz);
        if (tasks == null) {
            completedTasks.putIfAbsent(clazz, Sets.<String>newConcurrentHashSet());
            tasks = completedTasks.get(clazz);
        }
        if (tasks.contains(taskType)) {
            return false;
        }
        synchronized (Ones.class) {
            if (tasks.contains(taskType)) {
                return false;
            }
            doOnce.doOne(clazz);
            tasks.add(taskType);
        }
        return true;
    }
}
