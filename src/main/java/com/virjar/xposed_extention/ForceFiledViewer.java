package com.virjar.xposed_extention;

import android.os.Bundle;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;

import java.lang.reflect.Array;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentMap;

/**
 * Created by virjar on 2018/9/25.<br>
 * 强制的，将一个对象的内部属性dump出来，忽略java pojo的getter规范，请注意这个功能是为了抓取使用，因为该转换不是幂等的，无法还原
 */
public class ForceFiledViewer {
    /**
     * build a plain object from any flex object,the output is a view ,witch access all private and public field,ignore getter on input object
     *
     * @param input any object
     * @return a view,just contain string,number,list,map;or it`s combination
     */
    public static Object toView(Object input) {
        return toView(input, Sets.newHashSet(), false);
    }

    public static Object toView(Object input, boolean withObjType) {
        return toView(input, Sets.newHashSet(), withObjType);
    }

    private static Object trimView(Object input) {
        if (input == null) {
            return null;
        }
        if (input instanceof Map && ((Map) input).size() == 0) {
            return null;
        }
        if (input.getClass().isArray() && Array.getLength(input) == 0) {
            return null;
        }
        if (input instanceof Collection && ((Collection) input).size() == 0) {
            return null;
        }
        return input;
    }

    private static Object toView(Object input, Set<Object> accessedObjects, boolean withObjType) {
        try {
            if (input == null) {
                return Collections.emptyMap();
            }
            if (skipTranslatePackage.isSubPackage(input.getClass().getName())) {
                return input;
            }
            return toViewInternal(input, accessedObjects, withObjType);
        } catch (Throwable throwable) {
            return null;
        }
    }

    private static Object toViewInternal(Object input, Set<Object> accessedObjects, boolean withObjType) {
        if (input == null) {
            return null;
        }
        Class<?> inputClass = input.getClass();
        if (inputClass.isPrimitive() || ReflectUtil.wrapperToPrimitive(inputClass) != null) {
            return input;
        }

        if (accessedObjects.contains(input)) {
            return null;
        }
        accessedObjects.add(input);
        if (input instanceof CharSequence || input instanceof Number) {
            return input;
        }
        if (input instanceof Date) {
            return ((Date) input).getTime();
        }

        if (input instanceof Collection) {
            List<Object> subRet = Lists.newArrayListWithCapacity(((Collection) input).size());
            boolean hint = false;
            for (Object o1 : (Collection) input) {
                Object view = trimView(toView(o1, accessedObjects, withObjType));
                if (view != null) {
                    hint = true;
                }
                subRet.add(view);
            }
            if (!hint) {
                subRet.clear();
            }
            return subRet;
        }
        if (input instanceof byte[]) {
            return input;
            //不要自作聪明
//            if (((byte[]) input).length > 128) {
//                //二进制数据，直接略过不处理
//                return "byte stream data";
//            } else {
//                return input;
//            }
        }
        if (input.getClass().isArray()) {
            List<Object> subRet = Lists.newArrayListWithCapacity(Array.getLength(input));
            boolean hint = false;
            for (int i = 0; i < Array.getLength(input); i++) {
                Object o1 = Array.get(input, i);
                Object view = trimView(toView(o1, accessedObjects, withObjType));
                if (view != null) {
                    hint = true;
                }
                subRet.add(view);
            }
            if (!hint) {
                subRet.clear();
            }
            return subRet;
        }


        if (input instanceof Map) {
            Map map = (Map) input;
            Map<String, Object> subRet = Maps.newTreeMap();
            Set set = map.entrySet();
            for (Object entry : set) {
                if (!(entry instanceof Map.Entry)) {
                    continue;
                }
                Map.Entry entry1 = (Map.Entry) entry;
                Object key = entry1.getKey();
                if (key == null) {
                    continue;
                }
                Object value = entry1.getValue();
                Object view = trimView(toView(value, accessedObjects, withObjType));
                if (view != null) {
                    if (key instanceof CharSequence || key.getClass().isPrimitive() || ReflectUtil.wrapperToPrimitive(key.getClass()) != null) {
                        subRet.put(key.toString(), view);
                    } else if (key instanceof Class) {
                        subRet.put(((Class) key).getName(), view);
                    } else {
                        Map<String, Object> container = Maps.newHashMap();
                        container.put("key", trimView(toView(value, accessedObjects, withObjType)));
                        container.put("value", value);
                        subRet.put(key.getClass() + "@" + key.hashCode(), container);
                    }


                }
            }
            return subRet;
        }
        String className = input.getClass().getName();
        if (skipDumpPackage.isSubPackage(className) && !forceDumpPackage.isSubPackage(className)) {
            //框架内部对象，不抽取，这可能导致递归过深，而且没有意义
            return null;
        }
        if (Bundle.class.isAssignableFrom(input.getClass())) {
            Bundle bundle = (Bundle) input;
            //触发反序列化
            bundle.get("test");
        }

        Map<String, Object> ret = Maps.newTreeMap();

        Field[] fields = classFileds(input.getClass());
        for (Field field : fields) {
            Object o = null;
            try {
                o = field.get(input);
            } catch (IllegalAccessException e) {
                //ignore
            }
            if (o == null) {
                if (withObjType) {
                    Map<String, Object> container = Maps.newHashMap();
                    container.put("type", field.getType());
                    container.put("value", null);
                    ret.put(field.getName(), container);
                }
                continue;
            }
            Object view = trimView(toView(o, accessedObjects, withObjType));
            if (view != null) {
                if (withObjType) {
                    Map<String, Object> container = Maps.newHashMap();
                    container.put("type", o.getClass());
                    container.put("value", view);
                    container.put("hash", o.hashCode());
                    ret.put(field.getName(), container);
                } else {
                    ret.put(field.getName(), view);
                }
            } else if (withObjType) {
                Map<String, Object> container = Maps.newHashMap();
                container.put("type", field.getType());
                container.put("value", null);
                container.put("hash", o.hashCode());
                ret.put(field.getName(), container);
            }
        }
        return ret;
    }


    public static Field[] classFileds(Class clazz) {
        if (clazz == Object.class) {
            return new Field[0];
        }
        Field[] fields = fieldCache.get(clazz);
        if (fields != null) {
            return fields;
        }
        synchronized (clazz) {
            fields = fieldCache.get(clazz);
            if (fields != null) {
                return fields;
            }
            ArrayList<Field> ret = Lists.newArrayList();
            ret.addAll(Arrays.asList(clazz.getDeclaredFields()));
            ret.addAll(Arrays.asList(classFileds(clazz.getSuperclass())));
            Iterator<Field> iterator = ret.iterator();
            while (iterator.hasNext()) {
                Field next = iterator.next();
                if (Modifier.isStatic(next.getModifiers())) {
                    iterator.remove();
                    continue;
                }
                if (next.isSynthetic()) {
                    iterator.remove();
                    continue;
                }
                if (!next.isAccessible()) {
                    next.setAccessible(true);
                }
            }
            fields = ret.toArray(new Field[0]);

            fieldCache.put(clazz, fields);
        }
        return fields;
    }

    private static final ConcurrentMap<Class, Field[]> fieldCache = Maps.newConcurrentMap();
    private static PackageTrie forceDumpPackage = new PackageTrie();
    private static PackageTrie skipDumpPackage = new PackageTrie();
    private static PackageTrie skipTranslatePackage = new PackageTrie();

    public static void addForceViewConfig(String basePackage) {
        forceDumpPackage.addToTree(basePackage);
    }

    public static void addSkipViewConfig(String basePackage) {
        skipDumpPackage.addToTree(basePackage);
    }

    public static void addSKipTranslateConfig(String basePackage) {
        skipTranslatePackage.addToTree(basePackage);
    }

    static {
        addSkipViewConfig("android");
        addSkipViewConfig("com.android");
        addSkipViewConfig("java");
        addSkipViewConfig("dalvik");

        addForceViewConfig("android.os.Bundle");
        addForceViewConfig("android.os.BaseBundle");
        addForceViewConfig("android.util.ArrayMap");
        addForceViewConfig("android.util.SparseArray");
        addForceViewConfig("android.util.ArraySet");
        addForceViewConfig("android.util.SparseBooleanArray");
        addForceViewConfig("android.util.SparseIntArray");
        addForceViewConfig("android.util.SparseLongArray");
        addForceViewConfig("android.util.StateSet");
        addForceViewConfig("android.content.Intent");
        addForceViewConfig("android.content.ClipData");
        addForceViewConfig("android.content.ClipData#Item");
        addForceViewConfig("android.content.ClipData.Item");
        addForceViewConfig("android.content.ComponentName");
        addForceViewConfig("android.content.Entity");
        addForceViewConfig("android.content.ContentValues");

        addSKipTranslateConfig("com.alibaba.fastjson");
        addSKipTranslateConfig("org.json");
        addSKipTranslateConfig("com.google.gson");
    }
}
